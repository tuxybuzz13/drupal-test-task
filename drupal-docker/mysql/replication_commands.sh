mysql# flush tables with read lock;
mysql# show master status;
root# mysqldump --all-databases --user=root --password --master-data > everything.sql
root# zip -r everything.zip everything.sql
mysql# unlock tables;