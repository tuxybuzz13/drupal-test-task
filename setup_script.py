#!/usr/bin/python

import argparse
import subprocess
import logging
import os
import time
import shutil


def install_pre_requisites():
    """Install Pre Requistes for the drupal docker setup"""
    try:
        subprocess.check_call(['yum', 'install', 'docker', 'curl', '-y'])
        subprocess.check_call(['curl', '-L',
            'https://github.com/docker/compose/releases/download/1.9.0/docker-compose-linux-x86_64',
            '-o', '/usr/bin/docker-compose'])
        subprocess.check_call(['chmod', '+x', '/usr/bin/docker-compose'])
        subprocess.check_call(['service', 'docker', 'start'])
    except subprocess.CalledProcessError:
        logging.error("Problem Installing Pre-Requisites...Rolling back")
        rollback_pre_requisites()


def rollback_pre_requisites():
    """Remove Pre-Requisites and Return System to original state"""
    try:
        subprocess.check_call(['yum', 'remove', 'docker', '-y'])
        os.remove("/usr/bin/docker-compose")
        shutil.rmtree("/var/lib/docker")
    except:
        logging.error("PROBLEM Rolling Back, please intervene manually")


def configure_mysql_replication():
    """Create an initial snapshot and prepare the master for replication"""
    try:
        subprocess.check_call(['docker', 'exec', '-it', 'db0', 'mysql', '-u',
            'root', '-pdreeHigCuWievAvdocLawokCy', '-e',
            'flush tables with read lock'])
        with open('mysql/replication_status', 'w') as replication_status:
            subprocess.check_call(['docker', 'exec', '-it', 'db0', 'mysql', '-u',
                'root', '-pdreeHigCuWievAvdocLawokCy', '-e',
                'show master status'], stdout=replication_status)
        with open('mysql/snapshot_file.sql', 'w') as snapshot_file:
            subprocess.check_call(['docker', 'exec', '-it', 'db0', 'mysqldump', '--all-databases',
                '-u', 'root', '-pdreeHigCuWievAvdocLawokCy', '--master-data'], stdout=snapshot_file)
        subprocess.check_call(['docker', 'exec', '-it', 'db0', 'mysql', '-u',
            'root', '-pdreeHigCuWievAvdocLawokCy', '-e',
            'unlock tables'])
    except:
        logging.error("Problem Configuring Database for replication. Rolling Back")
        rollback_pre_requisites()


def start_up_containers():
    """Build and start the containers"""
    os.chdir('drupal-docker')
    try:
        subprocess.check_call(['docker-compose', 'up', '-d'])
    except:
        logging.error("Problem Starting Containers. Rolling back all changes")
        # rollback_containers() # No-need as removing docker will take care of that
        rollback_pre_requisites()


def main():
    install_pre_requisites()
    start_up_containers()
    # Give the containers some time to fully come up
    time.sleep(25)
    configure_mysql_replication()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    main()